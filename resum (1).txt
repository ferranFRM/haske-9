NOM PEÇA + POSICIO ACTUAL (minuscula+numero) + POSICIO SEGUENT

				Possibles moviments?
P per peça				 Cap endavant, 2 al principi, 1 despres
C per cavall			 L, 8 possibilitats
A per �lfil				diagonal, qualsevol...
T per torre				 horitzontal, vertical, qualsevol...
D per dama (o reina)	 horitzontal, vertical, diagonal, ...
R per rei.				 1, 8 possibilitats

Escac 	    = + 								//el rei esta amenacat
Escac i mat = ++								//capturar el rei
Peo corona, alfinal possem = i la pe�a resultant (Ex: Pb7b8=D)
Enroc curt  = 0-0
Enroc llarg = 0-0-0


Peon	Mata de lado, si tiene algo delante no se puede mover
Alfil	No puede moverse si tiene alguna en medio
Torre 	=
Dama	No puede saltar
Rei 	No puede ponerse donde haya una pieza aliada
Caballo Puede SALTAR, no puede moverse si hay una pieza del mismo color en el destino


Coses a fer:
- Advertir escac si el rei es amenaçat
- En cas que un jugador posi el rei en escac, es il·legal i no es pot fer la jugada
- Quan a un rei se li fa escac, el jugador ha de:
	- Capturar la peça amenaçant si esta en posicio de ferho, i si es la unica peca que amenaca al rei.
	- Moure el rei a una casella lliure d'escac.
	- Posar una peca davant del rei que impedeixi la captura (tapar)

Estats finals:
- Escac i mat: un jugador guanya quan pot capturar al rei
- Taules: empat
- Adversari abandona: abandona
etc, mirar pdf